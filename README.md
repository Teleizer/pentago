# README #
### What is this repository for? ###

The game pentago in python.

### How do I get set up? ###

Run Pentago.py with python 3

Example if I am player b
then my move: 3/6 1R will change the following board:
---------------
|. . w | . . .|
|. . . | . . .|
|. . . | . . .|
===============
|. . . | . . .|
|. . . | . . .|
|. . . | . . .|
---------------

To: 
---------------
|. . . | . . .|
|. . . | . . .|
|. . w | . . .|
===============
|. . . | . . .|
|. . b | . . .|
|. . . | . . .|
---------------
